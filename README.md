# pyscripts

Diese Skripte sind ein erster Anfang, um die Zugriff auf die Sectigo-API zu automatisieren.
Hauptsächlich beziehen diese sich auf die Validierung von Domains.

Alle Skripte können mit -h aufgerufen werden, um eine Kurzhilfe zu erhalten.

Die Skripte verwenden ein Schlüsselbund, um Zugangsdaten zu speichern (was über das Modul credentials.py gemanaged wird). Falls noch keine Zugansdaten im Schlüsselbund vorhanden sind, werden diese abgefragt und gespeichert.

Ist kein Schlüsselbund vorhanden, muss (momentan noch) das Passwort als Parameter angegeben werden. Dieses ist lediglich für vollautomatisierte Umgebungen gedacht, wo die Zugangsdaten aus einer separaten Datei gelesen werden.

Bei den Start-/SubmitValidation* Skripten muss immer zuerst das StartValidation aufgerufen werden. Dieses gibt entsprechende Anweisungen aus. Dann muss das zugehörige SubmitValidation aufgerufen werden.

