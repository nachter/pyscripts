#!/usr/bin/env python3
import getopt, sys
import requests
import argparse
import credentials

def main():
    """"""
    parser = argparse.ArgumentParser(description="Zeige den Validations-Status einer Domain")
    parser.add_argument('-d', '--domain', dest='domain', required=True, help='Abzufragende Doamin')
    parser.add_argument('-l', '--login', dest='login', default = 'MeinSectigoLogin', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    args = parser.parse_args()

    
    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    customerUri = 'DFN'

    Api = "https://cert-manager.com/api/dcv/v2/validation/status"
    my_headers = {'login': login, 'password': passphrase, 'customerUri': customerUri }

    body = {"domain":args.domain}

    response = requests.post(Api, headers=my_headers, json=body )

    print(response.text)

if __name__ == "__main__":
    main()

