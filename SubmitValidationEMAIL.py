#!/usr/bin/env python3
import getopt, sys
import credentials
import requests
import argparse
import json

def main():
    """"""
    parser = argparse.ArgumentParser(description="Uebermittele eine Validierung an Sectigo.")
    parser.add_argument('-d', '--domain', dest='domain', required=True, help='Zu validierende Doamin')
    parser.add_argument('-e', '--email', dest='email', required=True, help='Die zur Validierung genutzte Email. Diese muss unterhalb der Domain liegen.')
    parser.add_argument('-l', '--login', dest='login', default = 'achtergarde_api', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    args = parser.parse_args()

    
    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    customerUri = 'DFN'
    domain = args.domain
    email = args.email
    
    Api = "https://cert-manager.com/api/dcv/v1/validation/submit/domain/email"
    my_headers = {'Content-Type': 'application/json;charset=utf-8', 'login': login, 'password': passphrase, 'customerUri': customerUri }
    body = {'domain': domain, 'email': email }
    response = requests.post(Api, headers=my_headers, json=body)
    print("Response_Code: " + str(response.status_code))
    
if __name__ == "__main__":
    main()

