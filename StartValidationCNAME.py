#!/usr/bin/env python3
import getopt, sys
import credentials
import requests
import argparse
import json

def main():
    """"""
    parser = argparse.ArgumentParser(description="Starte die Validierung einer Domain per CNAME. Bitte beachten, dass die Validierung fuer die Hauptdomain durchgefuehrt werden muss, also z.B. fuer programmregister.de, nicht fuer www.programmregister.de")
    parser.add_argument('-d', '--domain', dest='domain', required=True, help='Zu validierende Doamin')
    parser.add_argument('-l', '--login', dest='login', default = 'achtergarde_api', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    args = parser.parse_args()

    
    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    customerUri = 'DFN'

    Api = "https://cert-manager.com/api/dcv/v1/validation/start/domain/cname"
    my_headers = {'Content-Type': 'application/json;charset=utf-8', 'login': login, 'password': passphrase, 'customerUri': customerUri }
    body = {'domain': args.domain }
    response = requests.post(Api, headers=my_headers, json=body)
    jresp = json.loads(response.content)
    
    print("Erstelle einen CNAME DNS Eintrag wie folgt:")
    print(jresp['host'] + " CNAME " + jresp['point'])
    print("")
    print("Fuehre danach das Skript SubmitValidationCNAME.py aus, um die Validierung abzuschließen.")

if __name__ == "__main__":
    main()

