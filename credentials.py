import keyring

def get_credentials(login):
    """"""
    passphrase = keyring.get_password("sectigo-api", login)
    if passphrase is None:
        passphrase=input("Es existiert kein Eintrag im Keyring. Bitte die Passphrase für "+login+" eingeben: ")
        keyring.set_password("sectigo-api", login, passphrase)
    return passphrase

