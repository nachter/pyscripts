#!/usr/bin/env python3
import getopt, sys
import credentials
import requests
import argparse
import json

def main():
    """"""
    parser = argparse.ArgumentParser(description="Zeige die Domains, wo die Validierung bis expirein ablaeuft.")
    parser.add_argument('-e', '--expirein', dest='expiresin', default='100', help='Tage, in denen die Validierung der Domains auslaeuft (default 100)')
    parser.add_argument('-l', '--login', dest='login', default = 'MeinSectigoLogin', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    args = parser.parse_args()

    
    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    customerUri = 'DFN'

    Api = "https://cert-manager.com/api/dcv/v1/validation?expiresIn="+args.expiresin+"&size=100"
    my_headers = {'login': login, 'password': passphrase, 'customerUri': customerUri }

    response = requests.get(Api, headers=my_headers )

    jresp = json.loads(response.content)
    for domain in jresp:
        print(domain["domain"], domain["dcvMethod"], domain["expirationDate"])

if __name__ == "__main__":
    main()

