#!/usr/bin/env python3
import getopt, sys
import credentials
import requests
import argparse
import json

def main():
    """"""
    parser = argparse.ArgumentParser(description="Starte die Validierung einer Domain per EMAIL. Bitte beachten, dass die Validierung fuer die Hauptdomain durchgefuehrt werden muss, also z.B. fuer programmregister.de, nicht fuer www.programmregister.de")
    parser.add_argument('-d', '--domain', dest='domain', required=True, help='Zu validierende Doamin')
    parser.add_argument('-l', '--login', dest='login', default = 'achtergarde_api', help='Login fuer die Sectigo-API')
    parser.add_argument('-p', '--passphrase', dest='passphrase', help='Passphrase fuer das Login')
    args = parser.parse_args()

    
    login = args.login
    passphrase = args.passphrase
    if not all([passphrase]):
        passphrase = credentials.get_credentials(login)
    customerUri = 'DFN'
    domain = args.domain

    Api = "https://cert-manager.com/api/dcv/v1/validation/start/domain/email"
    my_headers = {'Content-Type': 'application/json;charset=utf-8', 'login': login, 'password': passphrase, 'customerUri': customerUri }
    body = {'domain': args.domain }
    response = requests.post(Api, headers=my_headers, json=body)
    jresp = json.loads(response.content)
    print("Verschicke die Validierungs-Email fuer die Domain " + domain + " bitte mit einem der folgenden Kommandos, abhaengig von der gewuenschten EMail-Adresse:")
    for mail in jresp['emails']:
        print("SubmitValidationEMAIL.py -d " + domain + " -e " + mail)

if __name__ == "__main__":
    main()

